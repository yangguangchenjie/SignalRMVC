﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataBase;
using Model;

namespace Business
{
    public class UserDataBase
    {
        /// <summary>
        /// 获取所有用户
        /// </summary>
        /// <returns></returns>
        public IList<User> GetUserList()
        {
            const string sql = "select * from [User]";
            return SqlMapperUtil.SqlWithParamsDapper<User>(sql, new { }).ToList();
        }

        /// <summary>
        /// 查询用户是否存在
        /// </summary>
        /// <param name="input">参数实体</param>
        /// <returns></returns>
        public bool SelectUserName(User input)
        {
            string sql = String.Format(@"SELECT * FROM dbo.[User] WHERE UserName = '{0}'", input.UserName);
            return SqlMapperUtil.SqlWithParamsSingle<User>(sql) != null;
        }

        /// <summary>
        /// 检查密码是否正确（正确登录,不正确提示）
        /// </summary>
        /// <param name="input">参数实体</param>
        /// <returns></returns>
        public bool SelectPassword(User input)
        {
            string sql = String.Format(@"SELECT * FROM dbo.[User] WHERE UserName = '{0}' AND Password='{1}' ", input.UserName, input.Password);
            return SqlMapperUtil.SqlWithParamsSingle<User>(sql) != null;
        }

        /// <summary>
        /// 获取当前用户的所有信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public User GetSelectUserName(User input)
        {
            string sql = String.Format(@"SELECT * FROM dbo.[User] WHERE UserName = '{0}'", input.UserName);
            return SqlMapperUtil.SqlWithParamsSingle<User>(sql, new { });
        }
    }
}
