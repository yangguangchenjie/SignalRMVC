﻿namespace Model
{
    public class User
    {
        /// <summary>
        /// 用户id
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// 用户姓名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 连接会话id
        /// </summary>
        public string ConnectionId { get; set; }

        /// <summary>
        /// 登录时间
        /// </summary>
        public string LastLoginTime { get; set; }

        /// <summary>
        /// 用户密码
        /// </summary>
        public string Password { get; set; }
    }
}
