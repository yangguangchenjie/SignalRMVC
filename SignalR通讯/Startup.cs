﻿using System.Web;
using System.Web.SessionState;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Owin;
using SignalR通讯;

[assembly: OwinStartup(typeof(Startup))]

namespace SignalR通讯
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // 有关如何配置应用程序的详细信息，请访问 http://go.microsoft.com/fwlink/?LinkID=316888

           // 重点,将MyUserFactory注入 主要用于私聊
            var userIdProvider = new MyUserFactory();
            GlobalHost.DependencyResolver.Register(typeof(IUserIdProvider), () => userIdProvider);

            //设置Webapi
            HttpContext.Current.SetSessionStateBehavior(SessionStateBehavior.Required);
            // 配置集线器
           app.MapSignalR();
        }
    }
}
