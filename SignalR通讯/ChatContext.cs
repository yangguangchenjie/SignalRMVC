﻿using Model;
using System.Collections.Generic;

namespace SignalR通讯
{
    /// <summary>
    /// 上下文类，用来模拟EF中的DbContext
    /// </summary>
    public class ChatContext
    {
        public List<UserGroup> Users { get; set; }

        public List<Connection> Connections { get; set; }

        public List<ChatRoom> Rooms { get; set; }

        public ChatContext()
        {
            Users = new List<UserGroup>();
            Connections = new List<Connection>();
            Rooms = new List<ChatRoom>();
        }
    }
}