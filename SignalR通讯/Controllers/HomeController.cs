﻿using System.Web;
using System.Web.Mvc;
using SignalR通讯.Tool;

namespace SignalR通讯.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            
            return View();
        }

        /// <summary>
        /// 私聊
        /// </summary>
        /// <returns></returns>
        public ActionResult About()
        {
            var user = CookieConfig.GetCookieValue("user");
            if (user != null)
            {
                Session["username"] = HttpUtility.UrlDecode(user.UserName);
                Session["userid"] = user.UserId;
                Session["connnectId"] = user.ConnectionId;
            }
            return View();
        }

        /// <summary>
        /// 所有人
        /// </summary>
        /// <returns></returns>
        public ActionResult Contact()
        {
            var user = CookieConfig.GetCookieValue("user");
            if (user != null)
            {
                Session["username"] = HttpUtility.UrlDecode(user.UserName);
                Session["userid"] = user.UserId;
            }
            return View();
        }

        /// <summary>
        /// 原有实例
        /// </summary>
        /// <returns></returns>
        public ActionResult Chat()
        {
            return View();
        }

        /// <summary>
        /// 群聊
        /// </summary>
        /// <returns></returns>
        public ActionResult GroupUser()
        {
            return View();
        }
    }
}