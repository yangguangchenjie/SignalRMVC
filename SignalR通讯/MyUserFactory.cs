﻿using Microsoft.AspNet.SignalR;

namespace SignalR通讯
{
    /// <summary>
    /// 私聊时候使用
    /// </summary>
    public class MyUserFactory : IUserIdProvider
    {
        /// <summary>
        /// 得到用户userid
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public string GetUserId(IRequest request)
        {
            if (request.GetHttpContext().Request.Cookies["user2"] != null)
            {
                var cookies = request.GetHttpContext().Request.Cookies["user2"];
                if (cookies != null)
                    return cookies.Value;
            }
            return "";
            // return Guid.NewGuid().ToString();
        }
    }
}