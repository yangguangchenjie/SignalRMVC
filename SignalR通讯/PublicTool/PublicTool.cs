﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using SignalR通讯.Controllers;

namespace SignalR通讯.PublicTool
{
    public static class PublicTool
    {
        /// <summary>
        /// 自定义下拉框（获取登录用户的userid）
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="name">下拉框id名</param>
        /// <param name="obj">定义的样式</param>
        /// <param name="selectedVal">选中值</param>
        /// <param name="needDefault">是否有请选择</param>
        /// <returns></returns>
        public static MvcHtmlString SendDropDownList(
           this  HtmlHelper htmlHelper,
            string name,
            object obj,
            dynamic selectedVal = null,
             bool needDefault = true)
        {

            var industry = LogoController.OnlineUsers;
            var list = new List<SelectListItem>();
            if (needDefault)
            {
                list.Add(new SelectListItem
                {
                    Value = "",
                    Text = @"===请选择==="
                });
            }
            list.AddRange(industry.Select(o => new SelectListItem { Text = HttpUtility.UrlDecode(o.UserName), Value = o.UserId.ToString() }));
            if (selectedVal != null)
            {
                var item = list.Find(o => o.Value == selectedVal.ToString());
                if (item != null)
                    item.Selected = true;
            }
            return htmlHelper.DropDownList(name, list, obj);
        }
    }
}